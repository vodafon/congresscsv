require 'active_record'
require 'csv'
require './models/bill'

class Stats
  class << self
    def get_stats(argv)
      if not argv[1].include? "csv"
        puts "arguments for stats: stats file.csv congress_id"
        exit
      end
      csv_text = File.read(argv[1])
      csv = CSV.parse(csv_text)
      how_many_bills_matches(csv, argv[2])
    end

    def how_many_bills_matches(csv, congress)
      sum_titles = 0
      group_arr = split_arr_blank csv
      group_arr.each do |group|
        if have_title_match(group)
          sum_titles += 1
        end
      end
      percent = group_arr.size.to_f / Bill.where(:congress => congress.to_i).count * 100
      puts "How many bills #{congress} congress have matches: #{group_arr.size}"
      puts "How many bills #{congress} congress have at least one title match: #{sum_titles}"
      puts "How many bills #{congress} congress have only summary matches: #{group_arr.size - sum_titles}"
      puts "What % of total bills #{congress} congress had at least one match: #{"%.02f" % percent}%"
    end

    private
    def have_title_match(group)
      have_title = false
      group.each do |item|
        have_title = true if item.last.include?("Title")
      end
      have_title
    end

    def split_arr_blank(arr)
      new_arr = []
      tmp_arr = []
      arr.each_with_index do |ar, i|
        if ar.size == 0
          new_arr << tmp_arr
          tmp_arr = []
        elsif i == (arr.size - 1)
          tmp_arr << ar
          new_arr << tmp_arr
          tmp_arr = []
        else
          tmp_arr << ar
        end
      end
      new_arr
    end

  end
end
