require './lib/import'
require './lib/matching'
require './lib/stats'

class MainApp
  class << self
    def import(argv)
      arr = argv.map {|ar| ar if ar.include? "csv"}.compact.uniq
      puts "no valid files" if arr.blank?
      arr.each do |filepath|
        Import.import_csv filepath
      end
    end

    def matching(argv)
      if argv.size < 4 || !(argv[3].include? "csv")
        puts "for matching arguments format: congess_id, congress_id, output.csv"
        exit
      else
        Matching.matching_congresses argv[1].to_i, argv[2].to_i, argv[3]
      end
    end

    def stats(argv)
      Stats.get_stats(argv)
    end
  end
end
