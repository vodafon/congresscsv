require 'active_record'
require 'csv'
require './models/bill'

class Import
  class << self
    def import_csv(filepath)
      puts "Start import from #{filepath}"
      csv_text = File.read(filepath)
      csv = CSV.parse(csv_text, :headers => true)
      csv.each do |csv_item|
        create_bill_from_hash(csv_item.to_hash)
      end
      puts "Import from #{filepath} success"
    end
    
    private

    def create_bill_from_hash(csv_hash)
      bill = Bill.new
      bill.bill_id = csv_hash.fetch("Id", "0").to_i
      bill.chamber = csv_hash.fetch("Chamber", nil)
      bill.congress = csv_hash.fetch("Congress", "0").to_i
      bill.created_at = csv_hash.fetch("Created at").to_datetime
      bill.updated_at = csv_hash.fetch("Updated at").to_datetime
      bill.official_title = csv_hash.fetch("Official title", "")
      bill.official_title_trans = title_trans(csv_hash.fetch("Official title", ""))
      bill.official_title_trans_count = bill.official_title_trans.size
      bill.summary = csv_hash.fetch("Summary", "")
      bill.summary_trans = title_trans(bill.summary)
      bill.summary_trans_count = bill.summary_trans.size
      bill.row = csv_hash.map {|cs| cs[1]}
      bill.save
    end

    def title_trans(str)
      str.to_s.strip.downcase.gsub(/\W/, "")
    end
  end
end
