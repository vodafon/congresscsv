require 'active_record'
require 'csv'
require './models/bill'

class Matching
  class << self

    def matching_congresses(congress1, congress2, filepath)
      arr = []
      Bill.where(:congress => congress1).each do |bill|
        ret = find_equal_from_bill(bill, congress2)
        if ret.size > 1
          ret.each do |re|
            arr << re
          end
        end
      end
      headers = get_headers << "Matches"
      write_data_to_csv(filepath, headers, arr)
    end

    def get_headers
      csv_text = File.read("files/headers.csv")
      csv = CSV.parse(csv_text)
      csv.first
    end

    def find_equal_from_bill(bill, congress)
      bills = Bill.where(:congress => congress, :official_title_trans => bill.official_title_trans).to_a
      if bill.summary_trans_count > 0
        ids_to_exclude = bills.map {|b| b.id}
        bills_table = Arel::Table.new(:bills)
        Bill.where(:congress => congress, :summary_trans => bill.summary_trans).where(bills_table[:id].not_in ids_to_exclude).to_a.each do |bi|
          bills << bi
        end
      end
      arr = check_bills(bills, bill)
      if arr.size > 1
        arr << []
      end
      return arr
    end

    def check_bills(bills, bill)
      arr = []
      arr << bill.row
      bills.each do |bi|
        matches = ""
        matches += "Title" if bill.official_title_trans == bi.official_title_trans
        matches += "Summary" if bill.summary_trans_count > 0 && bill.summary_trans == bi.summary_trans
        arr << (bi.row << matches) if matches != ""
      end
      return arr
    end

    def write_data_to_csv(filepath, headers, arr)
      CSV.open(filepath, "w") do |csv|
        csv << headers
        arr.each do |ar|
          csv << ar
        end
      end
    end

  end
end
