require 'active_record'
require 'yaml'

require './lib/main_app'

if ARGV.size < 2
  puts "arguments must be 2 or more"
  exit
end

dbconfig = YAML::load(File.open(File.join(File.dirname(__FILE__), 'config', 'databases.yml')))
ActiveRecord::Base.establish_connection(dbconfig['development'])

case ARGV[0]
when 'import'
  MainApp.import ARGV
when 'matching'
  MainApp.matching ARGV
when 'stats'
  MainApp.stats ARGV
else
  puts "Function #{ARGV[0]} not found"
  exit
end
