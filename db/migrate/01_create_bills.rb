class CreateBills < ActiveRecord::Migration
  def up
    create_table :bills do |t|
      t.integer :bill_id
      t.string :chamber
      t.integer :congress
      t.text :official_title
      t.text :official_title_trans
      t.integer :official_title_trans_count
      t.text :summary
      t.text :summary_trans
      t.integer :summary_trans_count
      t.text :row, array: true, default: [], uniq: true


      t.boolean :active
      t.boolean :enacted
      t.boolean :vetoed
      t.boolean :summarized

      t.timestamps
    end

    add_index(:bills, :bill_id)
    add_index(:bills, :congress)
    add_index(:bills, :official_title_trans)
  end

  def down
    remove_index(:bills, :official_title_trans)
    remove_index(:bills, :congress)
    remove_index(:bills, :bill_id)
    drop_table :bills
  end
end
