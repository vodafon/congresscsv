# Congress CSV #

Matches and stats for bills.

### How use? ###

1. Install gems 
```
#!ruby

bundle install
```

2. Copy config/databases.yml.example to config/databases.yml and config it.

3. Import files to database:

```
#!ruby

$ruby congresscsv.rb import path/to/file1.csv
$ruby congresscsv.rb import path/to/file2.csv
```

4. Create matches file (format congess_id, congress_id, output.csv).
ex:

```
#!ruby

$ruby congresscsv.rb matching 112 113 tmp/matches.csv
```

5. Get stats from output file (format path/to/matches/file.csv congress_id)
ex:

```
#!ruby

$ruby congresscsv.rb stats tmp/matches.csv 112
```
