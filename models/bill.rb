class Bill < ActiveRecord::Base
  validates_presence_of :bill_id, :congress, :official_title_trans, :row
  validates_uniqueness_of :row
end
